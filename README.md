fncensus
========
A script that reports how many files in a folder's subdirectories contain a substring, or a given file extension.

Output is in CSV format, sorted from minimum to maximum in order of files found. Fields are comma seperated but also tab-indented for clarity, which may complicate importing to spreadsheet software if desired; use the "-p" command line flag for "plain" csv without tabs.

Optionally, export can be in JSON format instead, which might be handy for use in a downstream script or function. Use the "-j" flag.

Usage example: python3 fncensus ~/Music "mp3"
(On Linux there is no need to explicitly call python3)

Output:
```
Count,	Path
...
33,	Declan O'Rourke
47,	The Dandy Warhols
52,	Compilations
```